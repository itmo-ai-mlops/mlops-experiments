import json

import torch
from fire import Fire

from scripts.model_inference import Resnet18
from scripts.utils import prepare_data

model = Resnet18(num_labels=12)
DEVICE = "cpu"
state_dict = torch.load("./weights/model_fixed.pt", map_location=torch.device(DEVICE))
model.load_state_dict(state_dict)
model.eval()

with open("labels.json", "r") as f:
    labels_raw = json.loads(f.read())
    labels = {int(index): labels_raw[index] for index in labels_raw}


def main(path_to_image: str):
    tensor_image = prepare_data(path_to_image)
    index = model(tensor_image).argmax().item()
    label = labels[index]
    with open("./data/out/label.txt", "w") as file:
        file.writelines(label)
    return


if __name__ == "__main__":
    Fire(main)
