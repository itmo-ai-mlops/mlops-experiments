import os

import hydra
import lightning.pytorch as pl
import mlflow
import torch
from omegaconf import DictConfig

from scripts.data import MyDataModule
from scripts.model import MyModel
from scripts.utils import logger_init


@hydra.main(config_path="conf", config_name="config_train", version_base="1.3")
def main(cfg: DictConfig):
    os.environ["OMP_NUM_THREADS"] = str(cfg.train.threads)
    torch.set_num_threads(cfg.train.threads)

    pl.seed_everything(cfg.model.seed)
    logger = logger_init(cfg.data.logger_name)
    mlflow.pytorch.autolog()

    logger.info("\n\n\n ### TRAIN NEW INSTANCE MODEL ###")

    torch.set_float32_matmul_precision(cfg.train.mantissa)

    dm = MyDataModule(
        conf=cfg,
        logger=logger,
    )

    num_labels = len(dm.get_classes())

    logger.info(f"Num labels: {num_labels}")

    model = MyModel(conf=cfg, logger=logger, num_labels=num_labels)

    loggers = [
        pl.loggers.CSVLogger("logs/my-csv-logs", name=cfg.artifacts.experiment_name),
        pl.loggers.MLFlowLogger(
            experiment_name=cfg.artifacts.experiment_name,
            tracking_uri="http://localhost:5001",
            run_name=cfg.artifacts.run_name,
        ),
    ]

    callbacks = [
        pl.callbacks.LearningRateMonitor(logging_interval="step"),
    ]

    if cfg.callbacks.swa.use:
        callbacks.append(
            pl.callbacks.StochasticWeightAveraging(swa_lrs=cfg.callbacks.swa.lrs)
        )

    if cfg.artifacts.checkpoint.use:
        callbacks.append(
            pl.callbacks.ModelCheckpoint(
                dirpath=os.path.join(
                    cfg.artifacts.checkpoint.dirpath, cfg.artifacts.experiment_name
                ),
                filename=cfg.artifacts.checkpoint.filename,
                monitor=cfg.artifacts.checkpoint.monitor,
                save_top_k=cfg.artifacts.checkpoint.save_top_k,
                # every_n_train_steps=cfg.artifacts.checkpoint.every_n_train_steps,
                every_n_epochs=cfg.artifacts.checkpoint.every_n_epochs,
                mode="max",
                save_weights_only=True,
            )
        )

    trainer = pl.Trainer(
        accelerator=cfg.train.accelerator,
        # devices=cfg.train.devices,
        precision=cfg.train.precision,
        max_epochs=cfg.train.num_epochs,
        # accumulate_grad_batches=cfg.train.grad_accum_steps,
        # val_check_interval=cfg.train.val_check_interval,
        overfit_batches=cfg.train.overfit_batches,
        num_sanity_val_steps=cfg.train.num_sanity_val_steps,
        deterministic=cfg.train.full_deterministic_mode,
        benchmark=cfg.train.benchmark,  # может ускорить, а может нет (данные константы по входу)
        gradient_clip_val=cfg.train.gradient_clip_val,
        profiler=cfg.train.profiler,
        log_every_n_steps=cfg.train.log_every_n_steps,
        detect_anomaly=cfg.train.detect_anomaly,  # хорошая штука для дебага модели
        enable_checkpointing=cfg.artifacts.checkpoint.use,
        logger=loggers,
        callbacks=callbacks,
    )

    if cfg.train.batch_size_finder:
        tuner = pl.tuner.Tuner(trainer)
        tuner.scale_batch_size(model, datamodule=dm, mode="power")
    with mlflow.start_run():
        trainer.fit(model, datamodule=dm)
        logger.info("save model to a mlflow")
        mlflow.pytorch.log_model(model, "model_1")


if __name__ == "__main__":
    main()
