import logging
from typing import Optional

import lightning.pytorch
import matplotlib.pyplot as plt
import omegaconf
import torch
from PIL import Image
from torch.utils.data import random_split
from torchvision import datasets, transforms


class MyDataModule(lightning.pytorch.LightningDataModule):
    """A DataModule standardizes the training, val, test splits, data preparation and
    transforms. The main advantage is consistent data splits, data preparation and
    transforms across models.
    """

    def __init__(
        self,
        conf: omegaconf.dictconfig.DictConfig,
        logger: logging.Logger,
    ):
        super().__init__()
        self.save_hyperparameters()
        self.conf = conf
        self.logger = logger

        self.data_dir = self.conf["data"]["image_path"]
        self.seed = self.conf["model"]["seed"]

        self.labels = self.get_classes()

        self.train_transform = transforms.Compose(
            [
                transforms.RandomHorizontalFlip(p=0.5),
                transforms.RandomVerticalFlip(p=0.5),
                transforms.RandomApply(
                    torch.nn.ModuleList([transforms.ColorJitter()]), p=0.1
                ),
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
            ]
        )

        self.test_transform = transforms.Compose(
            [
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
            ]
        )

    def setup(self, stage: Optional[str] = None):
        """Called at the beginning of fit (train + validate), validate, test, or predict.
        This is a good hook when you need to build models dynamically or adjust something
        about them. This hook is called on every process when using DDP.
        """
        self.logger.info("SETUP DATA")
        all_data = datasets.ImageFolder(self.data_dir, transform=self.train_transform)
        self.train_data_len = int(len(all_data) * 0.75)
        self.test_data_len = int(len(all_data) - self.train_data_len)
        (
            self.train_data,
            _,
        ) = random_split(all_data, [self.train_data_len, self.test_data_len])

        all_data = datasets.ImageFolder(self.data_dir, transform=self.test_transform)
        _, self.test_data = random_split(
            all_data, [self.train_data_len, self.test_data_len]
        )
        self.logger.info(f"Train data len {self.train_data_len}")
        self.logger.info(f"Test data len {self.test_data_len}")

    def train_dataloader(self) -> torch.utils.data.DataLoader:
        """An iterable or collection of iterables specifying training samples"""
        self.logger.info("GET TRAIN DATALOADER")
        return torch.utils.data.DataLoader(
            dataset=self.train_data,
            batch_size=self.conf["model"]["batch_size"],
            num_workers=self.conf["data"]["dataloader_num_wokers"],
            drop_last=True,
            shuffle=True,
            pin_memory=False,
        )

    def val_dataloader(self) -> torch.utils.data.DataLoader:
        """An iterable or collection of iterables specifying validation samples."""
        self.logger.info("GET TEST DATALOADER")
        return torch.utils.data.DataLoader(
            dataset=self.test_data,
            batch_size=self.conf["model"]["batch_size"],
            num_workers=self.conf["data"]["dataloader_num_wokers"],
            drop_last=False,
            shuffle=False,
            pin_memory=False,
        )

    def teardown(self, stage: str) -> None:
        """Called at the end of fit (train + validate), validate, test, or predict.

        Args:
            stage: either ``'fit'``, ``'validate'``, ``'test'``, or ``'predict'``
        """
        pass

    def get_classes(
        self,
    ):
        all_data = datasets.ImageFolder(self.data_dir)
        return all_data.classes

    def check_test_dataset(self, number_observation: int | None = None):
        if number_observation:
            assert (
                number_observation < self.test_data_len
            ), "Number observation greater than a len test data"
            observation = self.test_data[number_observation]
            plt.title(self.labels[observation[1]])
            plt.imshow(observation[0].permute(1, 2, 0).numpy())
            plt.show()

        else:
            number_observation = 0
            observation = self.test_data[number_observation]
            plt.title(self.labels[observation[1]])
            plt.imshow(observation[0].permute(1, 2, 0).numpy())
            plt.show()

    def check_train_dataset(self, number_observation: int | None = None):
        if number_observation:
            assert (
                number_observation < self.train_data_len
            ), "Number observation greater than a len train data"
            observation = self.train_data[number_observation]
            plt.title(self.labels[observation[1]])
            plt.imshow(observation[0].permute(1, 2, 0).numpy())
            plt.show()

        else:
            number_observation = 0
            observation = self.train_data[number_observation]
            plt.title(self.labels[observation[1]])
            plt.imshow(observation[0].permute(1, 2, 0).numpy())
            plt.show()

    def preprocessing_for_fit(self, image: str) -> torch.tensor:
        image = Image.open(image)
        image = self.test_transform(image).unsqueeze(0)
        return image
