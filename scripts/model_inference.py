import torch
import torchvision
from torch import nn


class Resnet18(torch.nn.Module):
    def __init__(self, num_labels: int):
        super().__init__()
        self.resnet18 = torchvision.models.efficientnet_b2(pretrained=False)
        n_inputs = self.resnet18.classifier[1].in_features

        self.resnet18.classifier = nn.Sequential(
            nn.Linear(n_inputs, 2048),
            nn.SiLU(),
            nn.Dropout(0.3),
            nn.Linear(2048, num_labels),
        )

    def forward(self, x):
        outputs = self.resnet18(x)
        return outputs
