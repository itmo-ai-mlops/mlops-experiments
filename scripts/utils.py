import logging

import torchvision.transforms as transforms
from PIL import Image

transform_pipeline = transforms.Compose(
    [
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ]
)


def logger_init(file_path: str = "log.log"):
    logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")
    logger = logging.getLogger(__name__)
    file_handler = logging.FileHandler(file_path)
    file_handler.setLevel(logging.INFO)
    logger.addHandler(file_handler)
    return logger


def prepare_data(img_path):
    image = Image.open(img_path)
    img_data = transform_pipeline(image).unsqueeze(0)
    return img_data
