import logging
from typing import Any

import lightning.pytorch as pl
import omegaconf
import torch
import torchvision
import transformers
from torch import nn
from torcheval.metrics.functional import multiclass_f1_score
from transformers import get_linear_schedule_with_warmup


class Resnet18(torch.nn.Module):
    def __init__(self, conf: omegaconf.dictconfig.DictConfig, num_labels: int):
        super().__init__()
        self.conf = conf
        self.resnet18 = torchvision.models.efficientnet_b2(pretrained=True)
        if conf["model"]["freeze_backbone"]:
            for param in self.resnet18.parameters():
                param.requires_grad = False
        n_inputs = self.resnet18.classifier[1].in_features

        self.resnet18.classifier = nn.Sequential(
            nn.Linear(n_inputs, 2048),
            nn.SiLU(),
            nn.Dropout(0.3),
            nn.Linear(2048, num_labels),
        )

    def forward(self, x):
        outputs = self.resnet18(x)
        return outputs


class MyModel(pl.LightningModule):
    def __init__(
        self,
        conf: omegaconf.dictconfig.DictConfig,
        logger: logging.Logger,
        num_labels: int,
    ):
        super().__init__()
        self.save_hyperparameters()
        self.resnet18 = Resnet18(conf, num_labels)
        self.conf = conf
        self.my_logger = logger

        self.initializer_range = conf["model"]["initializer_range"]

        # loss
        # loss_class_weight = ...
        self.loss_fn_train = torch.nn.CrossEntropyLoss(
            # weight=loss_class_weight,
            label_smoothing=conf["model"]["label_smoothing"],
        )

        # metrics
        self.count_batches = 0
        self.accuracy = 0
        self.f1_score = 0
        self.total = 0

    def forward(self, input_ids):
        return self.resnet18(input_ids)

    def training_step(self, batch: Any, batch_idx: int, dataloader_idx=0):
        """Here you compute and return the training loss and some additional metrics
        for e.g. the progress bar or logger.
        """
        input_ids, labels = batch

        batch_size = input_ids.shape[0]

        outputs = self.forward(input_ids)

        loss = self.loss_fn_train(outputs, labels)

        _, preds_labels = torch.max(outputs, 1)

        accuracy = ((preds_labels == labels).sum() / batch_size).item()
        f1_score = multiclass_f1_score(preds_labels, labels, average="micro").item()

        self.log(
            "train_loss",
            loss / batch_size,
            logger=True,
            on_step=True,
            on_epoch=True,
            prog_bar=True,
        )

        self.log(
            "train_accuracy",
            accuracy,
            logger=True,
            on_step=True,
            on_epoch=True,
            prog_bar=True,
        )

        self.log(
            "train_f1_score",
            f1_score,
            logger=True,
            on_step=True,
            on_epoch=True,
            prog_bar=True,
        )

        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        input_ids, labels = batch

        batch_size = input_ids.shape[0]

        self.count_batches += 1

        outputs = self.forward(input_ids)

        loss = self.loss_fn_train(outputs, labels)

        _, preds_labels = torch.max(outputs, 1)

        self.accuracy += ((preds_labels == labels).sum() / batch_size).item()
        self.f1_score += multiclass_f1_score(
            preds_labels, labels, average="micro"
        ).item()
        self.total += loss / batch_size

        self.log(
            "val_loss", loss / batch_size, logger=True, on_epoch=True, prog_bar=True
        )
        return loss

    def on_validation_epoch_end(self):
        self.log(
            "val_total", self.total / self.count_batches, logger=True, on_epoch=True
        )
        self.log(
            "test_accuracy",
            self.accuracy / self.count_batches,
            logger=True,
            on_epoch=True,
        )
        self.log(
            "test_f1_score",
            self.f1_score / self.count_batches,
            logger=True,
            on_epoch=True,
        )
        self.total = 0
        self.accuracy = 0
        self.f1_score = 0
        self.count_batches = 0

    def test_step(self, batch: Any, batch_idx: int, dataloader_idx: int = 0):
        """Operates on a single batch of data from the test set.
        In this step you'd normally generate examples or calculate anything of interest
        such as accuracy.
        """
        pass

    def predict_step(
        self,
        batch: Any,
        batch_idx: int | None = None,
        dataloader_idx: int = 0,
        mode: str = "cpu",
    ) -> Any:
        """Step function called during
        :meth:`~pytorch_lightning.trainer.trainer.Trainer.predict`. By default, it
        calls :meth:`~pytorch_lightning.core.module.LightningModule.forward`.
        Override to add any processing logic.
        """
        self.my_logger.info("Predict step")
        input_ids = batch
        input_ids = input_ids.to(mode)
        outputs = self.forward(input_ids)

        _, preds_labels = torch.max(outputs, 1)

        return preds_labels.item()

    def configure_optimizers(self):
        weight_decay = self.conf["train"]["weight_decay"]
        no_decay = ["bias", "LayerNorm.bias", "LayerNorm.weight"]
        param_optimizer = list(self.resnet18.named_parameters())
        optimizer_grouped_parameters = [
            {
                "params": [
                    p for n, p in param_optimizer if not any(nd in n for nd in no_decay)
                ],
                "weight_decay": weight_decay,
            },
            {
                "params": [
                    p for n, p in param_optimizer if any(nd in n for nd in no_decay)
                ],
                "weight_decay": 0.0,
            },
        ]
        optim = torch.optim.AdamW(
            optimizer_grouped_parameters, lr=self.conf["train"]["learning_rate"]
        )
        if self.conf["train"]["scheduler"] == "linear":
            scheduler = get_linear_schedule_with_warmup(
                optim,
                num_warmup_steps=self.conf["train"]["num_warmup_steps"],
                num_training_steps=self.conf["train"]["num_training_steps"],
            )
        elif self.conf["train"]["scheduler"] == "cosine":
            scheduler = transformers.get_cosine_schedule_with_warmup(
                optim,
                num_warmup_steps=1
                * self.len_train_dataloader
                // self.conf["train"]["accumulation_steps"],
                num_training_steps=10
                * self.len_train_dataloader
                // self.conf["train"]["accumulation_steps"],
            )
        return (
            {
                "optimizer": optim,
                "lr_scheduler": {
                    "scheduler": scheduler,
                    "interval": "step",
                    "frequency": 1,
                    "strict": True,
                },
            },
        )

    def on_before_optimizer_step(self, optimizer):
        self.log_dict(pl.utilities.grad_norm(self, norm_type=2))
        super().on_before_optimizer_step(optimizer)

    def init_weights(self):
        """Initialize the weights"""
        self.my_logger.info("init weights for model")
        for module in self.children():
            if isinstance(module, nn.Linear):
                # Slightly different from the TF version which uses truncated_normal for initialization
                # cf https://github.com/pytorch/pytorch/pull/5617
                module.weight.data.normal_(mean=0.0, std=self.initializer_range)
                if module.bias is not None:
                    module.bias.data.zero_()
            elif isinstance(module, nn.Embedding):
                # module.weight.data.normal_(mean=0.0, std=self.initializer_range)
                module.weight.data.uniform_(
                    -self.initializer_range, self.initializer_range
                )
                if module.padding_idx is not None:
                    module.weight.data[module.padding_idx].zero_()
            elif isinstance(module, nn.LayerNorm):
                module.bias.data.zero_()
                module.weight.data.fill_(1.0)

    def load_weights(self, weights_path):
        """Load weights from a checkpoint file."""
        self.my_logger.info(f"load weights for model from {weights_path}")
        checkpoint = torch.load(weights_path)
        self.load_state_dict(checkpoint["state_dict"])
