import joblib
import mlflow
import pandas as pd
from dotenv import load_dotenv
from fire import Fire
from sklearn.linear_model import LinearRegression

df = pd.read_csv("data/raw/data.csv")
mlflow.set_tracking_uri("http://127.0.0.1:13412")


def model(mode: str):
    if mode == "train":
        with mlflow.start_run():
            mlflow.sklearn.autolog()
            model = LinearRegression()
            model.fit(df[["X"]], df[["Y"]])
            joblib.dump(model, "weights/linear.pkl")
            # mlflow.sklearn.log_model(model, "linear-model")
            mlflow.end_run()

    elif mode == "inference":
        # model = mlflow.sklearn.load_model("linear-model")
        model = joblib.load("weights/linear.pkl")
        predict = model.predict(df[["X"]])
        df["predict"] = predict
        df.to_csv("data/out/output.csv")


if __name__ == "__main__":
    load_dotenv()
    Fire(model)
