import time
import requests
from tqdm import tqdm
from pathlib import Path
from bs4 import BeautifulSoup
from fake_headers import Headers
from requests import PreparedRequest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import SessionNotCreatedException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotInteractableException
from .utils import randomize_delay
from loguru import logger

parent_path = Path(__file__).parent.parent.resolve()


class Size:
    def __init__(self):
        self.large = "large"
        self.medium = "medium"
        self.small = "small"


class Orientation:
    def __init__(self):
        self.horizontal = "horizontal"
        self.vertical = "vertical"
        self.square = "square"


class ImageType:
    def __init__(self):
        self.photo = "photo"
        self.clipart = "clipart"
        self.lineart = "lineart"
        self.face = "face"
        self.demotivator = "demotivator"


class Color:
    def __init__(self):
        self.color = "color"
        self.gray = "gray"
        self.red = "red"
        self.orange = "orange"
        self.yellow = "yellow"
        self.cyan = "cyan"
        self.green = "green"
        self.blue = "blue"
        self.violet = "violet"
        self.white = "white"
        self.black = "black"


class Format:
    def __init__(self):
        self.jpg = "jpg"
        self.png = "png"
        self.gif = "gifan"


class Parser:
    def __init__(self):
        self.size = Size()
        self.orientation = Orientation()
        self.type = ImageType()
        self.color = Color()
        self.format = Format()

    def query_search(self,
                     query: str,
                     limit: int = 100,
                     delay: float = 6.0,
                     size: Size = None,
                     orientation: Orientation = None,
                     image_type: ImageType = None,
                     color: Color = None,
                     image_format: Format = None,
                     site: str = None) -> list:
        """
        Описание
        ---------
        Реализует функцию поиска по запросу в Яндекс Картинках.

        Параметры
        ---------
        **query:** str
                Текст запроса
        **limit:** int
                Требуемое (максимальное) количество изображений
        **delay:** float
                Время задержки между запросами (сек)
        **size:** Size
                Размер (большие, маленькие, средние)
        **orientation:** Orientation
                Ориентация (горизонтальная, вертикальная, квадрат)
        **image_type:** ImageType
                Тип искомых изображений (фото, лица, с белым фоном, ... )
        **color:** Color
                Цветовая гамма (ч/б, цветные, оранжевые, синие, ... )
        **image_format:** Format
                Формат изображений (jpg, png, gif)
        **site:** str
                Сайт, на котором расположены изображения

        Возвращаемое значение
        ---------
        list: Список URL-ссылок на изображения соответствующие запросу.
        """

        params = {"text": query,
                  "isize": size,
                  "iorient": orientation,
                  "type": image_type,
                  "icolor": color,
                  "itype": image_format,
                  "site": site,
                  "nomisspell": 1,
                  "noreask": 1,
                  "p": 0}

        return self.__get_images(params=params, limit=limit, delay=delay)

    def image_search(self,
                     url: str,
                     limit: int = 100,
                     delay: float = 6.0,
                     size: Size = None,
                     orientation: Orientation = None,
                     color: Color = None,
                     image_format: Format = None,
                     text: str = None,
                     site: str = None) -> list:
        """
        Описание
        ---------
        Реализует функцию поиска по изображению в Яндекс Картинках.

        Параметры
        ---------
        **url:** str
                URL-ссылка на изображение
        **limit:** int
                Требуемое (максимальное) количество изображений
        **delay:** float
                Время задержки между запросами (сек)
        **size:** Size
                Размер (большие, маленькие, средние)
        **orientation:** Orientation
                Ориентация (горизонтальная, вертикальная, квадрат)
        **color:** Color
                Цветовая гамма (ч/б, цветные, оранжевые, синие, ... )
        **image_format:** Format
                Формат изображений (jpg, png, gif, ... )
        **site:** str
                Сайт, на котором расположены изображения

        Возвращаемое значение
        ---------
        list: Список URL-ссылок на похожие изображения.
        """

        params = {"url": url,
                  "isize": size,
                  "iorient": orientation,
                  "icolor": color,
                  "itype": image_format,
                  "text": text,
                  "site": site,
                  "rpt": "imageview",
                  "cbir_page": "similar",
                  "p": 0}

        return self.__get_images(params=params, limit=limit, delay=delay)

    def __get_driver(self, ):
        try:
            options = webdriver.ChromeOptions()
            options.add_argument("--no-sandbox")
            options.add_argument('--headless')

            service = webdriver.ChromeService(executable_path=r"C:\WebDriver\chromedriver.exe")
            driver = webdriver.Chrome(service=service, options=options)
        except SessionNotCreatedException as e:
            logger.error(
                f"Ошибка: \033[91mSessionNotCreatedException.\033[0m Возможно, не установлен Chrome. \n\n{e.msg}")
            raise SystemExit(1)
        return driver

    def __execute_request(self, driver, request):
        try:
            driver.get(request.url)
        except WebDriverException as e:
            logger.error(f"Ошибка: \033[91mWebDriverException.\033[0m \n\n{e.msg}")
            raise SystemExit(1)

    def __get_images(self, params: dict, limit: int, delay: float):
        request = self.__prepare_request(params)  # Подготавливаем запрос
        driver = self.__get_driver()
        self.__execute_request(driver, request)

        urls = []
        pbar = tqdm(total=limit, desc="URL search...")
        card_index = 0
        while True:
            if len(driver.window_handles) > 1:
                driver.switch_to.window(driver.window_handles[0])
            cards = driver.find_elements(By.XPATH, "//a[@class='Link ContentImage-Cover']")
            for card in cards[card_index:]:
                try:
                    card.click()
                    card_index += 1
                    pbar.update(1)
                    if card_index > limit:
                        break
                except Exception:
                    cards = driver.find_elements(By.XPATH, "//a[@class='Link ContentImage-Cover']")
                html = driver.page_source

                pic_url = self.__parse_html(html)
                urls.append(pic_url)

                webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()

            if len(cards) == 0:
                pbar.set_postfix_str("Что-то пошло не так... Найдено 0 изображений.")
                break

            if len(cards) >= limit:
                break

            else:
                old_page_height = driver.execute_script("return document.body.scrollHeight")
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
                time.sleep(randomize_delay(delay))
                new_page_height = driver.execute_script("return document.body.scrollHeight")

                if old_page_height == new_page_height:
                    try:
                        driver.find_element(
                            By.XPATH,
                            "//button[@class='Button2 Button2_width_max Button2_size_l Button2_view_action "
                            "FetchListButton-Button']"
                        ).click()
                    except NoSuchElementException as e:
                        logger.error(f"Ошибка: {e.msg}")
                        break
                    except ElementNotInteractableException:
                        pbar.set_postfix_str("Найдено меньше изображений")
                        break
                    except Exception:
                        break

        driver.close()
        driver.quit()
        return urls[:limit]

    def __prepare_request(self, params: dict) -> PreparedRequest:
        """
        Описание
        ---------
        Подготавливает GET-запрос к Яндекс Картинкам с полученными параметрами
        и сгенерированными заголовками.

        Параметры
        ---------
        **params:** dict
                Параметры GET-запроса.

        Возвращаемое значение
        ---------
        PreparedRequest: Подготовленный GET-запрос.
        """

        params = {k: v for k, v in params.items() if v is not None}
        headers = Headers(headers=True).generate()

        request = requests.Request(method="GET",
                                   url="https://yandex.ru/images/search",
                                   params=params,
                                   headers=headers).prepare()
        return request

    def __parse_html(self, html: str) -> list:
        """
        Описание
        ---------
        Достаёт из html-кода страницы прямые ссылки на изображения.

        Параметры
        ---------
        **html:** str
                html-код страницы с изображениями.

        Возвращаемое значение
        ---------
        list: Список прямых URL-ссылок на изображения со страницы.
        """

        soup = BeautifulSoup(html, "html.parser")
        picture_place = soup.find("div", {"class": "OpenImageButton"})
        try:
            pic = picture_place.find("a", {"class": "Button2"})
            url = pic.get('href')
            return url
        except AttributeError:
            return None
