import os
import random
import requests
import shutil
from pathlib import Path
from tqdm import tqdm
from PIL import Image
from loguru import logger


def make_directory(dir_path: str):
    """Создаёт новую директорию. Если директория существует - перезаписывает."""
    try:
        os.makedirs(dir_path)
    except FileExistsError:
        shutil.rmtree(dir_path)
        os.makedirs(dir_path)


def remove_duplicates(urls: list) -> list:
    """Удаляет дублирующиеся URL-ссылки из списка, возвращает список уникальных ссылок"""

    unique_urls = []
    for url in urls:
        if url not in unique_urls:
            unique_urls.append(url)

    return unique_urls


def save_images(urls: list, dir_path: str, prefix: str = "", number_images: bool = False, timeout: float = 3.0):
    """
    Сохраняет изображения в заданную директорию

    **urls:** list
            Список URL-ссылок на изображения
    **dir_path:** str
            Путь, по которому будут сохранены изображения
    **prefix:** str
            Префикс к имени каждого изображения
    **number_images:** bool
            Если number_images = True, изображения будут пронумерованы перед сохранением
    **timeout:** float
            Время таймаута при сохранении изображения
    """

    if not os.path.exists(dir_path):
        make_directory(dir_path)

    broken_url_counter = 0
    ext_set = set(['jpg', 'png', 'jpeg', 'gif'])
    with tqdm(total=len(urls), desc="Downloading images...") as pbar:
        for i, url in enumerate(urls):
            try:
                image_name, ext = os.path.splitext(str(url.split('/')[-1]))
                image_name = prefix + image_name
                if ext not in ext_set:
                    ext = 'jpg'

                if number_images:
                    image_name = f"{i}_" + image_name

                path = os.path.join(dir_path, f'{image_name}.{ext}')

                r = requests.get(url=url, allow_redirects=True, timeout=timeout)
                open(path, 'wb').write(r.content)
            except Exception:
                broken_url_counter += 1
            pbar.update(1)

    logger.info(f"Сохранено изображений: {len(urls) - broken_url_counter}.\tНеудачно: {broken_url_counter}.\n")


def randomize_delay(delay: float) -> float:
    """Добавляет рандома в задержку. Возвращает полученное число измененное случайным образом на 15%."""
    return delay * random.uniform(0.85, 1.15)


def filter_broken_images(root):
    broken_count = 0
    good_images = 0
    for path, subdirs, files in tqdm(os.walk(root)):
        for name in files:
            image_path = os.path.join(path, name)
            try:
                img = Image.open(image_path)
                img.verify()
                img.close()
                good_images += 1
            except Exception:
                broken_count += 1
                os.remove(image_path)
    logger.info(
        f'Num broken images: {broken_count} \t\t '
        f'Images left {good_images}'
    )


def get_files(path, extensions=('*.jpg', '*.png')):
    all_files = []
    dp = Path(path)
    for ext in extensions:
        all_files.extend(dp.rglob(ext))
    return all_files


if __name__ == '__main__':
    filter_broken_images('images/fp_other')
